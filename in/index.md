---
title: Hello World
date: \today
keywords:
- hello
- intro
- about
- techblog
- blog
- devops
- hobbies
- about
- chess
- dota2
- rave
- photography
- bouldering
- scuba
- diving
- juggling
- spinning
- firestaff
- ledstaff
- frisbee
- dogs
---
Hello and welcome!

## About
I am Michael Zeevi, a Senior DevOps engineer, geek, and atheist. I'm passionate about Linux, technology (especially if it's <a href="https://en.wikipedia.org/wiki/Free_and_open-source_software" target="_blank">free/libre open-source software</a>), privacy, cyber-security, the natural sciences (physics, etc...), navigation and environmentalism.

The core of this site is my personal [technical blog](blog.html), in which I discuss and share various tips, tricks, approaches, insights and thoughts related to the technology and tools which I use at home and work, and various topics surrounding them.

## Hobbies
Apart from my passion for technology (above), I've collected ~~and dropped~~ many hobbies over the years... I'll try to list a few here that could be cool conversation starters or just interesting to ask about!

- I enjoy gaming - I play <a href="https://www.dota2.com/" target="_blank">_Dota2_</a> and enjoy watching its ESports events.
- I like Chess - you may send me a challenge via <a href="https://lichess.org/@/maze_88" target="_blank">lichess.org</a>!
- In regards to music - I enjoy exploring underground, fringe and experimental kinds of music (mostly electronic). I used to be quite active in my local rave scene and DJing (check out <a href="https://www.mixcloud.com/maze88/" target="_blank">my old _MixCloud_ profile</a>...). Oh and Metal is also wonderful...and underground Hip Hop, so is Punk and Ambient and many more!
- Today my artistic expression manifests itself in my amateur smartphone-based photography - take a look on [my photography page](photography.html) or <a href="https://pixelfed.social/maze88" target="_blank">Pixelfed account</a>.
- Physical activities I enjoy have included climbing (bouldering), SCUBA diving, skateboarding, spinning (LED & fire staff) and hiking. I'm also always happy to throw & catch a ~~Frisbee~~ disc.
- Other offline interests I enjoy are juggling (3-4 balls), speedcubing (see <a href="https://www.worldcubeassociation.org/persons/2019ZEEV01" target="_blank">my underwhelming WCA profile</a>) and some card magic.
- Additionally, my partner and I have two dogs which we enjoy cuddling and walking and a ~~baby~~ toddler boy! :3

## Contact
If you want to get in touch, ask anything, suggest feedback, collaborate or whatever then you can reach out to me using one of the contact methods listed at the bottom/left or on [my omg.lol](https://maze.omg.lol).

If you like what you see, feel free to <a href="https://liberapay.com/maze/donate" target="_blank">donate via Liberapay</a>!
