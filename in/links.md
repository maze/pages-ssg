---
title: Links
keywords:
- links
- misc
---
Below you can find a collection of links to various web pages that I found interesting for different reasons. I hope you'll find something novel in at least some of them!

- <a href="https://music.ishkur.com" target="_blank">https://music.ishkur.com</a><br>
  A highly detailed tree/branch diagram displaying the evolution of electronic music, detailing many subgenres, and including multiple example tracks per genre.
- <a href="https://radio.garden" target="_blank">https://radio.garden</a><br>
  Tune into thousands of live radio stations simply by pointing to them across a map of the globe.
- <a href="https://what-if.xkcd.com/archive/" target="_blank">https://what-if.xkcd.com/archive</a><br>
  Serious scientific answers to absurd hypothetical questions, illustraited by _xkcd_.
- <a href="https://commonvoice.mozilla.org" target="_blank">https://commonvoice.mozilla.org</a><br>
  Common Voice is a publicly available (CC license) voice dataset. You may contribute your voice (by reading short phrases) or validate voices (by listening to short recordings).
- <a href="https://thispersondoesnotexist.com" target="_blank">https://thispersondoesnotexist.com</a><br>
  Generates a human face, each time you refresh it. Sometimes it yields some creepy glitches, other times it's just eerily interesting.
- <a href="https://earth.nullschool.net" target="_blank">https://earth.nullschool.net</a><br>
  Displays a global live weather map and provides the viewer controls to adjust the data and display formats.
- <a href="http://www.textfiles.com" target="_blank">http://www.textfiles.com</a><br>
  Hosts an archive of some old-school internet content.
- <a href="http://www.milliondollarhomepage.com" target="_blank">http://www.milliondollarhomepage.com</a><br>
  Is another curious time capsule - the maker of this page sold each of its 1 million pixels (and their target link) for 1 USD...
- <a href="https://haveibeenpwned.com" target="_blank">https://haveibeenpwned.com</a><br>
  Lets you check if your details may have come up in any dumps of the all too common data breaches of our cyber age.
- <a href="https://trunkbaseddevelopment.com" target="_blank">https://trunkbaseddevelopment.com</a><br>
  Goes into good depth about the Trunk based Git branching method, interesting for DevOps engineers and developers.
