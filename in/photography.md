---
title: Photography
keywords:
- photography
- smartphone-photography
- pixelfed
- fediverse
- hobbies
- about
---
I upload my photography with <a href="https://pixelfed.org/" target="_blank">_Pixelfed_</a> - a free and ethical image sharing platform (read more related to this below).

Check it out below or follow me <a href="https://pixelfed.social/maze88" target="_blank">@maze88@pixelfed.social</a>!

<iframe src="https://pixelfed.social/maze88/embed" class="pixelfed__embed" style="max-width: 100%; border: 0px; overflow: auto;" width="710" allowfullscreen="allowfullscreen" loading="lazy" scrolling="yes" height="890"></iframe>
<script async defer src="https://pixelfed.social/embed.js"></script>
